import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { HomePage } from './home.page';

describe('HomePage', () => {
  let component: HomePage;
  let fixture: ComponentFixture<HomePage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ HomePage ],
      imports: [IonicModule.forRoot(), ReactiveFormsModule],
    }).compileComponents();

    fixture = TestBed.createComponent(HomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('get ngOnInit', () => {
    component.ngOnInit();
  });

  it('get send ok fields', () => {
    fixture.componentInstance.form.controls['email'].setValue('adsda@gmail.com');
    fixture.componentInstance.form.controls['password'].setValue('asdsadasasds');
    fixture.componentInstance.form.controls['remember'].setValue(false);

    component.send();
  });

  
  it('get send ko fields', () => {
    fixture.componentInstance.form.controls['email'].setValue('adsda@gmail.com');
    fixture.componentInstance.form.controls['password'].setValue('');
    fixture.componentInstance.form.controls['remember'].setValue(false);
    component.send();
  });
  
  it('login test invalid email empty password', () => {
    fixture.componentInstance.form.controls['email'].setValue('sfdfsfdfdssdf');
    fixture.componentInstance.form.controls['password'].setValue('');
    fixture.componentInstance.form.controls['remember'].setValue(false);
    component.send();
  });
  
  it('login test empty password valid email', () => {
    fixture.componentInstance.form.controls['email'].setValue('asdsadasdsd@gmail.com');
    fixture.componentInstance.form.controls['password'].setValue('');
    fixture.componentInstance.form.controls['remember'].setValue(false);
    component.send();
  });
  
  
  it('login test empty email invalid password', () => {
    fixture.componentInstance.form.controls['email'].setValue('');
    fixture.componentInstance.form.controls['password'].setValue('23');
    fixture.componentInstance.form.controls['remember'].setValue(false);
    component.send();
  });
  
  it('login test empty email valid password', () => {
    fixture.componentInstance.form.controls['email'].setValue('');
    fixture.componentInstance.form.controls['password'].setValue('234424324342');
    fixture.componentInstance.form.controls['remember'].setValue(false);
    component.send();
  });
  
  it('login test invalid email invalid password', () => {
    fixture.componentInstance.form.controls['email'].setValue('asdasd');
    fixture.componentInstance.form.controls['password'].setValue('234');
    fixture.componentInstance.form.controls['remember'].setValue(false);
    component.send();
  });
});