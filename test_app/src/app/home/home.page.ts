import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {

  warnings: Array<string> = [];
  form: FormGroup;
  photo: string = "";

  constructor(private formBuilder: FormBuilder) {}

  private initForm() {

    let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern(regexEmail)]],
      password: ['', [Validators.required, Validators.minLength(5)]],
      remember: [false]
    });
  }

  ngOnInit(): void {
    this.initForm();
    this.initUser();
  }

  resetErrors() {
    this.warnings = [];
  }

  send(): void {

    this.resetErrors();

    if (this.form.valid) {
      console.log("OK");
    } else {
      this.drawErrors();
    }
  }

    private drawErrors(): void {
    this.isEmpty('email');
    this.isEmpty('password');
    this.minPassword();
    this.formatEmail();
  }

  private isEmpty(field: string):void {
    if(this.form.controls[field].errors?.required) {
      this.warnings.push(
        field + " está sin rellenar."
      );
    }
  }

  private initUser(): void {
    this.photo = "../../assets/logo.jpg";
  }

  private minPassword():void { 
    const passField = this.form.controls['password'].errors?.minlength
    if(passField) {
      this.warnings.push(
        "La clave debe tener " + passField.requiredLength + " caracteres como mínimo."
      );
    }
  }

  private formatEmail():void {
    const emailField = this.form.controls['email'].errors?.pattern
    if(emailField) {
      this.warnings.push(
        "El formato del Email no es válido."
      );
    }
  }
}
